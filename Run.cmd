:: git --version



:: Set Credentials
:: git config --global user.email "primemirror@gmail.com"
:: git config --global user.email "git:https://gitlab.com"

:: git config --global user.name "ImperiaI"
:: git config --global user.password "98741236"
:: git config credential.helper store

ls -al ~/.ssh

:: git push -u origin master

:: git config --global credential.helper manager
:: git config --global credential.helper wincred
 
:: Clone via https
:: git clone https://gitlab.com/ImperiaI/Test.git

::  config --global credential.helper store
:: git pull

:: cd Test

:: Stage:
:: git add New.txt

:: Stage all *.txt files
:: git add *.txt                          

:: Stage all *.* unstaged files
:: git add *.*


:: Undo?
:: git restore --staged New.txt

:: Unstage
:: git rm --cached New.txt

:: Unstage All
:: git rm --cached *.*

:: Deletes file from working directory and stages deletion
:: git rm file


:: Create a new Branch
:: git branch Release

:: git tag Commit_ID


:: Merges the currently checked branch with:
:: git merge Release

:: Checkout Branchname/Master name
:: git checkout Master

:: List all remote repos
:: git remote


:: Manual Commit:
:: git commit
:: + i text + esc + :wq 

:: git commit -m "Added some Text"


:: Commit any staged files, probably no text notes.
:: git commit -a

:: push -u RemoteRepo from LocalRepo
:: git push -u origin master


:: git pull https://gitlab.com/ImperiaI/Test.git

:: Stow Status
:: git status

pause